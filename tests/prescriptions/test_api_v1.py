import json

import pytest


@pytest.mark.django_db
def test_prescription_api(requests_mock, settings, client):
    requests_mock.get(f'{settings.CLINIC_SERVICE_URL}/clinics/1', text='{"id": 1, "name": "Clínica A"}')
    requests_mock.get(f'{settings.PHYSICIAN_SERVICE_URL}/physicians/1',
                      text='{"id": 1, "name": "José", "crm": "SP293893"}')
    requests_mock.get(f'{settings.PATIENT_SERVICE_URL}/patients/1',
                      text='{"id": 1, "name": "Rodrigo", "email": "rodrigo@gmail.com", "phone": "(16)998765625"}')

    requests_mock.post(f'{settings.METRICS_SERVICE_URL}/metrics', text='''{
            "id": "1",
            "clinic_id": 1,
            "clinic_name": "Clinica A",
            "physician_id": 1,
            "physician_name": "Dr. João",
            "physician_crm": "SP293893",
            "patient_id": 1,
            "patient_name": "Rodrigo",
            "patient_email": "rodrigo@gmail.com",
            "patient_phone": "(16)998765625"
        }''')

    response = client.post('/prescriptions', json.dumps({
        "clinic": {
            "id": 1
        },
        "physician": {
            "id": 1
        },
        "patient": {
            "id": 1
        },
        "text": "Dipirona 1x ao dia"
    }), content_type="application/json")

    assert response.status_code in (200, 201)

    data = response.json()

    assert data['clinic']['id'] == 1
    assert data['physician']['id'] == 1
    assert data['patient']['id'] == 1
    assert data['text'] == 'Dipirona 1x ao dia'
