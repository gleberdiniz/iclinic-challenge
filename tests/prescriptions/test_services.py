import pytest
import requests.exceptions

from apps.prescriptions import errors
from apps.prescriptions.services import get_clinic, get_physician, get_patient, send_metrics


def test_get_clinic(requests_mock, settings):
    requests_mock.get(f'{settings.CLINIC_SERVICE_URL}/clinics/1', text='{"id": 1, "name": "Clínica A"}')

    clinic = get_clinic(1)

    assert clinic.get('id') == 1
    assert clinic.get('name') == 'Clínica A'


def test_get_clinic_not_found(requests_mock, settings):
    requests_mock.get(f'{settings.CLINIC_SERVICE_URL}/clinics/1', text='{}', status_code=404)

    clinic = get_clinic(1)

    assert clinic is None


def test_get_clinic_service_not_available(requests_mock, settings):
    requests_mock.get(f'{settings.CLINIC_SERVICE_URL}/clinics/1', exc=requests.exceptions.RetryError)

    clinic = get_clinic(1)

    assert clinic is None


def test_get_clinic_respect_cache_ttl(requests_mock, settings):
    settings.CACHES = {"default": {"BACKEND": "django.core.cache.backends.locmem.LocMemCache"}}
    requests_mock.get(f'{settings.CLINIC_SERVICE_URL}/clinics/1', text='{"id": 1, "name": "Clínica A"}')

    get_clinic(1)
    get_clinic(1)

    assert requests_mock.called
    assert requests_mock.call_count == 1


def test_get_physician(requests_mock, settings):
    requests_mock.get(f'{settings.PHYSICIAN_SERVICE_URL}/physicians/1',
                      text='{"id": 1, "name": "José", "crm": "SP293893"}')

    physician = get_physician(1)

    assert physician.get('id') == 1
    assert physician.get('name') == 'José'
    assert physician.get('crm') == 'SP293893'


def test_get_physician_not_found(requests_mock, settings):
    requests_mock.get(f'{settings.PHYSICIAN_SERVICE_URL}/physicians/1', text='{}', status_code=404)

    with pytest.raises(errors.PhysicianNotFoundError):
        get_physician(1)


def test_get_physician_service_not_available(requests_mock, settings):
    requests_mock.get(f'{settings.PHYSICIAN_SERVICE_URL}/physicians/1', exc=requests.exceptions.RetryError)

    with pytest.raises(errors.PhysiciansServiceNotAvailableError):
        get_physician(1)


def test_get_physician_respect_cache_ttl(requests_mock, settings):
    settings.CACHES = {"default": {"BACKEND": "django.core.cache.backends.locmem.LocMemCache"}}
    requests_mock.get(f'{settings.PHYSICIAN_SERVICE_URL}/physicians/1',
                      text='{"id": 1, "name": "José", "crm": "SP293893"}')

    get_physician(1)
    get_physician(1)

    assert requests_mock.called
    assert requests_mock.call_count == 1


def test_get_patient(requests_mock, settings):
    requests_mock.get(f'{settings.PATIENT_SERVICE_URL}/patients/1',
                      text='{"id": 1, "name": "Rodrigo", "email": "rodrigo@gmail.com", "phone": "(16)998765625"}')

    patient = get_patient(1)

    assert patient.get('id') == 1
    assert patient.get('name') == 'Rodrigo'
    assert patient.get('email') == 'rodrigo@gmail.com'
    assert patient.get('phone') == '(16)998765625'


def test_get_patient_not_found(requests_mock, settings):
    url = f'{settings.PATIENT_SERVICE_URL}/patients/1'
    requests_mock.get(url, text='{}', status_code=404)

    with pytest.raises(errors.PatientNotFoundError):
        get_patient(1)


def test_get_patient_service_not_available(requests_mock, settings):
    url = f'{settings.PATIENT_SERVICE_URL}/patients/1'
    requests_mock.get(url, exc=requests.exceptions.RetryError)

    with pytest.raises(errors.PatientsServiceNotAvailableError):
        get_patient(1)


def test_get_patient_respect_cache_ttl(requests_mock, settings):
    settings.CACHES = {"default": {"BACKEND": "django.core.cache.backends.locmem.LocMemCache"}}
    requests_mock.get(f'{settings.PATIENT_SERVICE_URL}/patients/1',
                      text='{"id": 1, "name": "Rodrigo", "email": "rodrigo@gmail.com", "phone": "(16)998765625"}')

    get_patient(1)
    get_patient(1)

    assert requests_mock.called
    assert requests_mock.call_count == 1


def test_send_metrics(requests_mock, settings):
    requests_mock.get(f'{settings.CLINIC_SERVICE_URL}/clinics/1', text='{"id": 1, "name": "Clínica A"}')
    requests_mock.get(f'{settings.PHYSICIAN_SERVICE_URL}/physicians/1',
                      text='{"id": 1, "name": "José", "crm": "SP293893"}')
    requests_mock.get(f'{settings.PATIENT_SERVICE_URL}/patients/1',
                      text='{"id": 1, "name": "Rodrigo", "email": "rodrigo@gmail.com", "phone": "(16)998765625"}')

    requests_mock.post(f'{settings.METRICS_SERVICE_URL}/metrics', text='''{
        "id": "1",
        "clinic_id": 1,
        "clinic_name": "Clinica A",
        "physician_id": 1,
        "physician_name": "Dr. João",
        "physician_crm": "SP293893",
        "patient_id": 1,
        "patient_name": "Rodrigo",
        "patient_email": "rodrigo@gmail.com",
        "patient_phone": "(16)998765625"
    }''')
    metrics = send_metrics(1, 1, 1)
    assert metrics.get('id') == '1'
    assert metrics.get('clinic_id') == 1
    assert metrics.get('physician_id') == 1
    assert metrics.get('patient_id') == 1
