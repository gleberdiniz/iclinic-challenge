from apps.prescriptions.models import Prescription


def test_prescription_model_exists():
    assert hasattr(Prescription, 'clinic_id')
    assert hasattr(Prescription, 'physician_id')
    assert hasattr(Prescription, 'patient_id')
    assert hasattr(Prescription, 'text')
