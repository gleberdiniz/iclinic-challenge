# iclinic-challenge

Desenvolvimento do desafio de Python da iClinic.

## Estrutura

Foi utilizado o framework Django em conjunto com o Django REST Framework (DRF) usando banco de dados PostgreSQL.
Tentei usar somente dependências que considero necessárias ou bem úteis.

A lógica principal está no arquivo apps.prescriptions.services. 

O método project.utils.exception_handler é a forma que o DRF tem para formatar as mensagens de erro. 

### Arquivos

O Django já possui uma arquitetura fixa que outros frameworks, apesar de ser possível mudar praticamente 
de tudo, eu prefiro manter o mais próximo dos padrões, vou explicar como os arquivos estão organizados, 
tanto os padrões do Django e DRF quanto os meus ajustes.

Eu versionei a url na arquitetura para mostrar como eu faria, mas mantive a url sem versionamento para seguir o que 
foi solicitado no desafio.

- **project:** sempre que vou criar um projeto django, crio com o nome de project, pois aí fica fácil identificar 
  onde estão as configurações do projeto

- **apps:** pasta onde as apps ficam agrupadas
  - **models.py:** onde são definidos os modelos
  - **services.py:** onde ficam o que é lógica de negócio
  - **migrations.py:** onde ficam as migrations da app
  - **admin.py:** definições do admin do django
  - **apps.py:** arquivo de configuração do django  
  - **api.v1:** costumo versionar toda a parte REST em vez de jogar os arquivos na pasta raiz da app
     - **urls.py:** define as urls dessa versão do app
     - **viewsets.py:** agrupa os viewsets do DRF
     - **serializers.py:** agrupa os serializers do DRF

- **tests:** pasta que contem todos os testes, eu prefiro uma pasta centralizada com testes de todas as apps e 
  do projeto
  
### Dependências

Estão definidas no arquivo requirements.txt, fixando cada versão utilizada.

- **Django REST Framework:** responsável por facilitar toda a parte de APIs REST
- **drf-spectacular:** Documentação swagger para as APIs
- **psycopg2-binary:** Lib para conexão com o PostgreSQL
- **python-decouple:** Uma lib que gosto de usar para definir configurações em através de variaveis de ambiente 
  ou arquivos .env
- **dj-database-url:** Assim como decouple, utilitário para passar a url de conexão do banco por 
- **requests:** utilizado para fazer as chamadas nos serviços dependentes
- **pytest e pytest-\*:** framework e plugins utilizados para realizar os testes

### Docker

Tentei deixar simples, fornecendo um Dockerfile com uma imagem slim e um docker-compose só com o Django e o 
PostgreSQL. 

Não gosto de colocar pra rodar migrations direto na imagem, pois isso impede de fazer deploy de duas instancias 
da mesma imagem.

Então para rodar as migrations dentro do docker-compose, execute o comando:

```shell
docker-compose run web ./manage.py migrate
```

### CI
Optei por hospedar e usar o CI do Gitlab, pois é a que tenho experiência.


## Passo a Passo de como rodar a aplicação

1. Execute o comando abaixo: 
```shell
docker-compose up
```
2. Aguarde o docker-compose subir os serviços db web
3. Execute o comando abaixo:
```shell
docker-compose run web ./manage.py migrate
```
4. Acesse http://localhost:8000/docs/ no navegador

Obs.: Caso não possua docker ou docker-compose instalado, siga as instruções nos links abaixo:
- docker: https://docs.docker.com/engine/install/
- docker-compose: https://docs.docker.com/compose/install/