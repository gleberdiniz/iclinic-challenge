from rest_framework.response import Response
from rest_framework.views import set_rollback

from apps.prescriptions.errors import PrescriptionError


def exception_handler(exc, context):
    set_rollback()

    if isinstance(exc, PrescriptionError):
        data = {
            "error": {
                "message": exc.msg,
                "code": exc.code
            }
        }
    else:
        # o DRF possui um formato diferente e incompativel com o formato de erro do desafio,
        # para manter no formato correto, qualquer erro do DRF como erros de validação ou de permissão,
        # ta retornando como "malformed request"

        data = {
            "error": {
                "message": 'malformed request',
                "code": '01'
            }
        }

    return Response(data, status=exc.status_code if hasattr(exc, 'status_code') else 400)
