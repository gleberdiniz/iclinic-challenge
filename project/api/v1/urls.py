from django.urls import path, include


urlpatterns = [
    path('', include('apps.prescriptions.api.v1.urls')),
    # path("api-auth/", include("rest_framework.urls", namespace="rest_framework")),
]
