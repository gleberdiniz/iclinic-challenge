from typing import Dict

from django.conf import settings
from requests.exceptions import RetryError

from .models import Prescription
from .utils import get_from_cache_or_service, request_with_retry
from . import errors


def create_prescription(clinic_id: int, physician_id: int, patient_id: int, text: str) -> Prescription:

    prescription = Prescription.objects.create(
        clinic_id=clinic_id,
        physician_id=physician_id,
        patient_id=patient_id,
        text=text
    )

    return prescription


def get_clinic(clinic_id: int) -> Dict:
    clinic = None

    try:
        clinic = get_from_cache_or_service(
            cache_key=f'clinic-{clinic_id}',
            service_url=f'{settings.CLINIC_SERVICE_URL}/clinics/{clinic_id}',
            service_auth=settings.CLINIC_SERVICE_AUTH,
            max_retries=3,
            service_timeout=5,
            cache_timeout_hours=72,
        )
    except (errors.ObjectNotFoundError, errors.ServiceNotAvailableError):
        pass

    return clinic


def get_physician(physician_id: int) -> Dict:
    try:
        physician = get_from_cache_or_service(
            cache_key=f'physician-{physician_id}',
            service_url=f'{settings.PHYSICIAN_SERVICE_URL}/physicians/{physician_id}',
            service_auth=settings.PHYSICIAN_SERVICE_AUTH,
            max_retries=2,
            service_timeout=4,
            cache_timeout_hours=48,
        )
    except errors.ObjectNotFoundError:
        raise errors.PhysicianNotFoundError()
    except errors.ServiceNotAvailableError:
        raise errors.PhysiciansServiceNotAvailableError()

    return physician


def get_patient(patient_id: int) -> Dict:
    try:
        patient = get_from_cache_or_service(
            cache_key=f'patient-{patient_id}',
            service_url=f'{settings.PATIENT_SERVICE_URL}/patients/{patient_id}',
            service_auth=settings.PATIENT_SERVICE_AUTH,
            max_retries=2,
            service_timeout=3,
            cache_timeout_hours=12,
        )
    except errors.ObjectNotFoundError:
        raise errors.PatientNotFoundError()
    except errors.ServiceNotAvailableError:
        raise errors.PatientsServiceNotAvailableError()

    return patient


def send_metrics(clinic_id: int, physician_id: int, patient_id: int) -> Dict:
    clinic = get_clinic(clinic_id)
    physician = get_physician(physician_id)
    patient = get_patient(patient_id)

    request_data = {
        'clinic_id': clinic_id,
        'physician_id': physician_id,
        'physician_name': physician['name'],
        'physician_crm': physician['crm'],
        'patient_id': patient_id,
        'patient_name': patient['name'],
        'patient_email': patient['email'],
        'patient_phone': patient['phone']
    }

    if clinic:
        request_data['clinic_name'] = clinic['name']

    try:
        response = request_with_retry(
            method='POST',
            service_url=f'{settings.METRICS_SERVICE_URL}/metrics',
            service_auth=settings.METRICS_SERVICE_AUTH,
            data=request_data,
            service_timeout=6,
            max_retries=5,
        )

    except RetryError:
        raise errors.MetricsServiceNotAvailableError()

    if response.status_code not in (200, 201):
        raise errors.MalformedRequestError()

    return response.json()
