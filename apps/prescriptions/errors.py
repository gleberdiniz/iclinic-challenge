
class PrescriptionError(Exception):
    code = '--'
    msg = 'prescription error'

    def __init__(self, *args, **kwargs):
        super().__init__(self.msg)


class ServiceNotAvailableError(PrescriptionError):
    code = 'na'
    msg = 'service not available'


class ObjectNotFoundError(PrescriptionError):
    code = 'nf'
    msg = 'object not found'


class MalformedRequestError(PrescriptionError):
    code = '01'
    msg = 'malformed request'


class PhysicianNotFoundError(ObjectNotFoundError):
    code = '02'
    msg = 'physician not found'


class PatientNotFoundError(ObjectNotFoundError):
    code = '03'
    msg = 'patient not found'


class MetricsServiceNotAvailableError(ServiceNotAvailableError):
    code = '04'
    msg = 'metrics service not available'


class PhysiciansServiceNotAvailableError(ServiceNotAvailableError):
    code = '05'
    msg = 'physicians service not available'


class PatientsServiceNotAvailableError(ServiceNotAvailableError):
    code = '06'
    msg = 'patients service not available'
