from django.db import models
from django.utils.translation import gettext_lazy as _


class Prescription(models.Model):
    clinic_id = models.IntegerField()
    physician_id = models.IntegerField()
    patient_id = models.IntegerField()
    text = models.TextField()

    class Meta:
        verbose_name = _('Prescription')
        verbose_name_plural = _('Prescriptions')

    def __str__(self):
        return f'{self.clinic_id} - {self.physician_id} - {self.patient_id} - {self.text}'

    @property
    def clinic(self):
        return {'id': self.clinic_id}

    @property
    def physician(self):
        return {'id': self.physician_id}

    @property
    def patient(self):
        return {'id': self.patient_id}
