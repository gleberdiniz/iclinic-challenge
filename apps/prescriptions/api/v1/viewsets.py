from rest_framework.viewsets import ModelViewSet

from apps.prescriptions.api.v1.permissions import PrescriptionPermission
from apps.prescriptions.api.v1.serializer import PrescriptionSerializer
from apps.prescriptions.models import Prescription


class PrescriptionViewSet(ModelViewSet):
    permission_classes = [PrescriptionPermission]
    serializer_class = PrescriptionSerializer

    def get_queryset(self):
        return Prescription.objects.all()

