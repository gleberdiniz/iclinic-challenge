from rest_framework.permissions import BasePermission


class PrescriptionPermission(BasePermission):
    def has_permission(self, request, view):
        if view.action in ('retrieve', 'list', 'create'):
            return True

        return False
