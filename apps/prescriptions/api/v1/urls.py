from django.urls import path, include
from rest_framework import routers

from . import viewsets

router = routers.DefaultRouter(trailing_slash=False)
router.register(r'prescriptions', viewsets.PrescriptionViewSet, basename='prescriptions')

urlpatterns = [
    path('', include(router.urls)),
]
