from django.db import transaction
from rest_framework import serializers

from apps.prescriptions import errors
from apps.prescriptions.models import Prescription
from apps.prescriptions.services import create_prescription, send_metrics


class ClinicSerializer(serializers.Serializer):
    id = serializers.IntegerField()


class PhysicianSerializer(serializers.Serializer):
    id = serializers.IntegerField()


class PatientSerializer(serializers.Serializer):
    id = serializers.IntegerField()


class PrescriptionSerializer(serializers.ModelSerializer):
    clinic = ClinicSerializer()
    physician = PhysicianSerializer()
    patient = PatientSerializer()

    class Meta:
        model = Prescription
        fields = ('id', 'clinic', 'physician', 'patient', 'text')

    def validate(self, attrs):
        for f in ('clinic', 'physician', 'patient', 'text'):
            if f not in attrs or (f != 'text' and not attrs[f].get('id')):
                raise errors.MalformedRequestError()

        return attrs

    def create(self, validated_data):

        clinic_id = validated_data['clinic']['id']
        physician_id = validated_data['physician']['id']
        patient_id = validated_data['patient']['id']

        with transaction.atomic():
            prescription = create_prescription(clinic_id, physician_id, patient_id, validated_data['text'])
            send_metrics(clinic_id, physician_id, patient_id)

        return prescription
