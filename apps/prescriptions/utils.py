import json
from typing import Dict

from django.core.cache import cache
from requests.adapters import HTTPAdapter
from requests.exceptions import RetryError
from urllib3 import Retry
import requests

from apps.prescriptions import errors

HOURS = 3600


def request_with_retry(method: str, service_url: str, service_auth: str, data: Dict = None, service_timeout: int = 0,
                       max_retries: int = 0):

    with requests.Session() as session:
        retries = Retry(total=max_retries, backoff_factor=0.1, status_forcelist=[500, 502, 503, 504])

        if service_url.startswith('https:'):
            session.mount('https://', HTTPAdapter(max_retries=retries))
        else:
            session.mount('http://', HTTPAdapter(max_retries=retries))

        headers = {
            'Authorization': service_auth,
        }

        if data:
            data = json.dumps(data)
            headers['Content-Type'] = 'application/json'

        response = session.request(method, service_url, data=data, timeout=service_timeout, headers=headers)

        return response


def get_from_cache_or_service(cache_key: str, service_url: str, service_auth: str, max_retries: int = 0,
                              service_timeout: int = 0, cache_timeout_hours: int = 0):
    data = cache.get(cache_key)

    if not data:

        try:
            response = request_with_retry('GET', service_url, service_auth, None, service_timeout, max_retries)

        except RetryError:
            raise errors.ServiceNotAvailableError()

        if response.status_code == 200:
            data = response.json()

            cache.set(cache_key, data, timeout=cache_timeout_hours * HOURS)
        elif response.status_code == 404:
            raise errors.ObjectNotFoundError()

    return data
